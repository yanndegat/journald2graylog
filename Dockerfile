FROM alpine:3.3

ENV DOCKER_BASE_VERSION=0.0.4

RUN addgroup graylog && \
    adduser -S -G graylog graylog

# Set up certificates, our base tools, and Consul.
RUN apk add --no-cache ca-certificates gnupg socat && \
    gpg --recv-keys 91A6E7F85D05C65630BEF18951852D87348FFC4C && \
    mkdir -p /tmp/build && \
    cd /tmp/build && \
    wget https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_linux_amd64.zip && \
    wget https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS && \
    wget https://releases.hashicorp.com/docker-base/${DOCKER_BASE_VERSION}/docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS.sig && \
    gpg --batch --verify docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS.sig docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS && \
    grep ${DOCKER_BASE_VERSION}_linux_amd64.zip docker-base_${DOCKER_BASE_VERSION}_SHA256SUMS | sha256sum -c && \
    unzip docker-base_${DOCKER_BASE_VERSION}_linux_amd64.zip && \
    cp bin/gosu bin/dumb-init /bin && \
    cd /tmp && \
    rm -rf /tmp/build && \
    apk del gnupg && \
    rm -rf /root/.gnupg

ENV GRAYLOG_HOSTNAME=graylog.example.com \
    GRAYLOG_GELF_PORT=12201 \
    GRAYLOG_GELF_PACKET_SIZE=1420 \
    LISTEN_PORT=22201 \
    CACERT=ca.pem \
    CERT=cert.pem \
    KEY=cert-key.pem \
    MAX_CONNECTIONS=50

VOLUME /certs

COPY ./build/journald2graylog /journald2graylog
COPY ./entrypoint.sh /
COPY ./proxy.sh /

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/proxy.sh"]
