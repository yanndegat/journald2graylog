package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/yanndegat/journald2graylog/journald"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

var (
	EndOfLog = []byte{0, 11, 0, 10}
	BUFFER_SIZE = 64 * 1024
	config      *Config
)

type Config struct {
	hostname         string
	verbose          bool
	enableRawLogLine bool

	graylogHostname  string
	graylogPort      int
	maxLogSize       int

	fields           Fields
	keepAlive        bool
	nodelay          bool
	usetls           bool
	tls              *tls.Config
	connection       Connection
}

type Fields map[string]string

// the flag.Value String ...
func (f *Fields) String() string {
	return fmt.Sprintf("%v", *f)
}

// the flag.Value Set(value string) error
func (f *Fields) Set(values string) error {
	fmap := *f
	tabFields := strings.Split(values, ";")
	for indice := range tabFields {
		key_value := strings.Split(tabFields[indice], "=")
		if len(key_value) == 2 {
			fmap[key_value[0]] = key_value[1]
		} else {
			return fmt.Errorf("error parsing fields %+v for key value : ù+v", values, key_value)
		}
	}
	return nil
}

type Connection interface {
	Write(data []byte) (int, error)
	Close() error
}

func init() {
	config = &Config{}
	config.fields = make(map[string]string)
	flag.BoolVar(&config.verbose, "v", false, "verbose mode (short)")
	flag.BoolVar(&config.verbose, "verbose", false, "verbose mode")

	flag.BoolVar(&config.enableRawLogLine, "rawjson", true, "send json rawlog")

	flag.BoolVar(&config.keepAlive, "keepalive", true, "enable keep alive")

	flag.BoolVar(&config.nodelay, "nodelay", true, "enable nodelay")

	flag.StringVar(&config.graylogHostname, "h", "127.0.0.1", "graylog host (short)")
	flag.StringVar(&config.graylogHostname, "host", "127.0.0.1", "graylog host")

	flag.IntVar(&config.graylogPort, "p", 12201, "graylog port (short)")
	flag.IntVar(&config.graylogPort, "port", 12201, "graylog port")

	flag.Var(&config.fields, "f", "additional fields (short)")
	flag.Var(&config.fields, "fields", "additional fields")

	flag.IntVar(&config.maxLogSize, "maxlogsize", 60 * 1024, "max log size")

	flag.BoolVar(&config.usetls, "tls", true, "tls connection")

	insecure := flag.Bool("insecure", false, "tls skip verify")

	tlsCertFile := flag.String("tlscert", "", "tls cert file path")

	tlsKeyFile := flag.String("tlskey", "", "tls key file path")

	flag.Parse()

	if config.usetls {
		config.tls = &tls.Config{InsecureSkipVerify: *insecure}
		flag.Parse()
		if *tlsKeyFile == "" || *tlsCertFile == "" {
			config.tls.ClientAuth = tls.ClientAuthType(0)
		} else {
			certificate, err := tls.LoadX509KeyPair(*tlsCertFile, *tlsKeyFile)
			if err != nil {
				log.Fatal(err)
			}
			config.tls.Certificates = []tls.Certificate{certificate}
			config.tls.ClientAuth = tls.ClientAuthType(tls.RequireAndVerifyClientCert)
		}
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	config.hostname = hostname

	if config.verbose {
		log.Printf("[DEBUG] Config is: %+v", config)
		if config.usetls {
			log.Printf("[DEBUG] Config tls is: %+v", *config.tls)
		}
	}
}

func connectTCP() (*net.TCPConn, error) {
	addr := fmt.Sprintf("%s:%d", config.graylogHostname, config.graylogPort)

	if config.verbose {
		log.Printf("[DEBUG] establishing tcp conn to addr %+v", addr)
	}
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}
	connTcp, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return nil, err
	}
	if err = connTcp.SetKeepAlive(config.keepAlive); err != nil {
		return nil, err
	}
	if err = connTcp.SetNoDelay(config.nodelay); err != nil {
		return nil, err
	}

	return connTcp, nil
}

func connectTLS() (*tls.Conn, error) {
	if config.verbose {
		log.Printf("[DEBUG] establishing tls tcp conn")
	}
	connTcp, err := connectTCP()
	if err != nil {
		return nil, err
	}
	conn := tls.Client(connTcp, config.tls)
	if config.verbose {
		log.Printf("[DEBUG] starting handshake")
	}
	err = conn.Handshake()
	if err != nil {
		conn.Close()
		return nil, err
	}
	if config.verbose {
		log.Printf("[DEBUG] handshake completed")
	}

	return conn, nil
}

func JsonToGelf(line string) ([]byte, error) {
	var logEntry journald.JournaldJSONLogEntry
	err := json.Unmarshal([]byte(line), &logEntry)
	if err != nil {
		return nil, err
	}

	var messageOut map[string]interface{}
	messageOut = make(map[string]interface{})
	messageOut["version"] = "1.1"

	// GELF: Hostname
	if logEntry.Hostname == "" || logEntry.Hostname == "localhost" {
		messageOut["host"] = config.hostname
	} else {
		messageOut["host"] = logEntry.Hostname
	}

	// GELF: Log Priority/Level
	messageOut["level"], err = strconv.Atoi(logEntry.Priority)
	if err != nil {
		panic(err)
	}

	// GELF: Message (ShortMessage)
	messageOut["short_message"] = logEntry.Message

	// GELF: Timestamp
	var jts = logEntry.RealtimeTimestamp
	messageOut["timestamp"], err = strconv.ParseFloat(fmt.Sprintf("%s.%s", jts[:10], jts[10:]), 64)

	// GELF: Facility
	if (logEntry.SyslogFacility != "") && (logEntry.SyslogIdentifier != "") {
		messageOut["facility"] = fmt.Sprintf("%s (%s)", logEntry.SyslogFacility, logEntry.SyslogIdentifier)
	} else if logEntry.SyslogFacility != "" {
		messageOut["facility"] = logEntry.SyslogFacility
	} else if logEntry.SyslogIdentifier != "" {
		messageOut["facility"] = logEntry.SyslogIdentifier
	} else {
		messageOut["facility"] = "Undefined"
	}

	// GELF: BootId
	messageOut["_BootID"] = logEntry.BootID

	// GELF: MachineId
	messageOut["_MachineID"] = logEntry.MachineID

	// GELF: PID
	messageOut["_PID"] = logEntry.PID
	// GELF: UID
	messageOut["_PID"] = logEntry.UID
	// GELF: GID
	messageOut["_GID"] = logEntry.GID

	// GELF: Executable
	messageOut["_Executable"] = logEntry.Executable
	// GELF: Command Line
	messageOut["_CommandLine"] = logEntry.CommandLine

	var lineNumber int
	if logEntry.CodeLine != "" {
		lineNumber, err = strconv.Atoi(logEntry.CodeLine)
		if err != nil {
			return nil, err
		}
		// GELF: Line
		messageOut["line"] = &lineNumber
		// GELF: File
		messageOut["file"] = logEntry.CodeFile
		// GELF: Function
		messageOut["_function"] = logEntry.CodeFunction
	}

	// GELF: Transport
	messageOut["_LogTransport"] = logEntry.Transport

	// GELF: Docker container id
	if logEntry.ContainerId != "" {
		messageOut["_CONTAINER_ID"] = logEntry.ContainerId
	}

	// GELF: Docker container id full
	if logEntry.ContainerIdFull != "" {
		messageOut["_CONTAINER_ID_FULL"] = logEntry.ContainerIdFull
	}

	// GELF: Docker container name
	if logEntry.ContainerName != "" {
		messageOut["_CONTAINER_NAME"] = logEntry.ContainerName
	}

	// GELF: Docker container tag
	if logEntry.ContainerTag != "" {
		messageOut["_CONTAINER_TAG"] = logEntry.ContainerTag
	}

	for key, value := range config.fields {
		messageOut[key] = value
	}

	// Populating the new GELF structure with all the data we received from
	// the journald's JSON formatted data from stdin.
	if config.enableRawLogLine {
		messageOut["_RawLogLine"] = line
	}

	var marshalledjson []byte
	marshalledjson, err = json.Marshal(messageOut)
	if err != nil {
		return nil, err
	}
	var buffercompact bytes.Buffer
	err = json.Compact(&buffercompact, marshalledjson)
	if err != nil {
		return nil, err
	}

	return buffercompact.Bytes(), nil
}

func main() {
	var conn Connection
	if config.usetls {
		tmp, err := connectTLS()
		if err != nil {
			panic(err)
		}
		conn = tmp
	} else {
		tmp, err := connectTCP()
		if err != nil {
			panic(err)
		}
		conn = tmp
	}
	defer conn.Close()

	if config.verbose {
		log.Println("[DEBUG] Reading from stdin...")
	}

	// Build the go reader of stdin from where the log stream will be piped
	reader := bufio.NewReaderSize(os.Stdin, BUFFER_SIZE)

	// Loop and process entries from stdin until EOF.
	for {
		line, err := reader.ReadString('\n')
		if config.verbose {
			log.Println("[DEBUG] messaage recieved")
		}

		if err == io.EOF {
			os.Exit(0)
		}
		if err != nil {
			log.Fatal(err)
		}

		gelfPayloadBytes, err := JsonToGelf(line)
		if err != nil {
			log.Printf("[WARN] cannot send line: %+v", err)
			continue
		}
		if len(gelfPayloadBytes) < config.maxLogSize {
			nbbytes, err := conn.Write(append(gelfPayloadBytes, EndOfLog...))
			if err != nil {
				panic(err)
			}

			if config.verbose {
				log.Printf("[DEBUG] sent log line of %d bytes\n", nbbytes)
				gelfPayload := string(gelfPayloadBytes)
				log.Printf("[DEBUG] line : %+v", gelfPayload)
			}
		} else {
			log.Printf("[WARN] Cannot send a too long message : %+v [truncated]", line[:100])
		}
	}
}