#!/bin/dumb-init /bin/sh
set -e

if [ -z "$GRAYLOG_HOSTNAME" ]; then
    echo "GRAYLOG_HOSTNAME is not set" >&2
    exit 1
fi

if [ -z "$GRAYLOG_GELF_PORT" ]; then
    echo "GRAYLOG_GELF_PORT is not set" >&2
    exit 1
fi

if [ -z "$GRAYLOG_GELF_PACKET_SIZE" ]; then
    echo "GRAYLOG_GELF_PACKET_SIZE is not set" >&2
    exit 1
fi

if [ -z "$LISTEN_PORT" ]; then
    echo "LISTEN_PORT is not set" >&2
    exit 1
fi

if [ -z "$CACERT" ] || [ ! -f "/certs/$CACERT" ]; then
    echo "CACERT is not set" >&2
    exit 1
fi

if [ -z "$CERT" ] || [ ! -f "/certs/$CERT" ]; then
    echo "CERT is not set" >&2
    exit 1
fi
if [ -z "$KEY" ] || [ ! -f "/certs/$KEY" ]; then
    echo "KEY is not set" >&2
    exit 1
fi

if [ -z "$MAX_CONNECTIONS" ]; then
    echo "MAX_CONNECTIONS is not set" >&2
    exit 1
fi

cp -R /certs /certs.graylog
chown -R graylog:graylog /certs.graylog

exec gosu graylog "$@"
